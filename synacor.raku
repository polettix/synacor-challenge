#!/usr/bin/env raku
use v6;
use JSON::Fast;
use Compress::Bzip2;

class SynacorChallengeVM { ... }
class SynacorChallengeVMHistory { ... }
class Digest::MD5 { ... }
sub MAIN (Str:D $filename = 'sample.bin', Str :$restore = '') {
   my $vm = SynacorChallengeVM.new;
   if $restore.chars {
      my $history = SynacorChallengeVMHistory.new.load($restore);
      $history.restore($vm);
      $vm.set-history($history);
   }
   else {
      if $filename ~~ 'json' {
         $vm.from-json($filename.IO.slurp);
      }
      else {
         my @code = load-program($filename);
         $vm.reset-to(@code);
      }
      my $history = SynacorChallengeVMHistory.new;
      $history.push($vm);
      $vm.set-history($history);
   }

   $vm.run;
}

sub load-program ($filename) {
   return [ $filename.IO.slurp.comb(/\d+/)».Int ]
      if $filename ~~ m{ \.txt $};
   my $stuff = $filename.IO.slurp(bin => True, close => True);
   return [
      gather {
         my $i = 0;
         while $i < $stuff.bytes {
            take $stuff.read-uint16($i, LittleEndian);
            $i += 2;
         }
      }
   ];
}

class SynacorChallengeVMHistory { ... }
class SynacorChallengeVM {
   class FatalException is Exception {
      has $.inner is built = Nil;
      has $.vm is built;
      has $.payload is built = '';
      method message {
         my $msg = $!payload;
         if $!inner {
            $msg ~= ' ' if $msg.chars;
            $msg ~= $!inner.message;
         }
         return $msg ~ ", at pc<{$!vm.pc}>";
      }
   }
   class AddressSpace {
      has $.n-items is built;
      has $.name is built;
      has @.data;

      class AddressSpaceException is Exception {}
      class AddressViolationException is AddressSpaceException {
         has $.address is built;
         has $.area is built;
         method message { "out of bounds access to {$!area}[$!address]" }
      }
      class ValueViolationException is AddressSpaceException {
         has $.address is built;
         has $.area    is built;
         has $.value   is built;
         method message { "invalid value <$!value>, while accessing {$!area}[$!address]" }
      }

      submethod TWEAK { @!data = 0 xx $!n-items }

      method all-values { @!data.map: { $_ //= 0 } }

      method read ($a) {
         AddressViolationException.new(address => $a, area => $!name).throw
            unless 0 <= $a < $!n-items;
         return @!data[$a] //= 0;
      }

      method write ($a, $v) {
         AddressViolationException.new(address => $a, area => $!name).throw
            unless 0 <= $a < $!n-items;
         ValueViolationException.new(address => $a, area => $!name, value => $v).throw
            unless 0 <= $v <= 0xFFFF;
         @!data[$a] = $v;
      }

      method init ($input = []) {
         @!data = 0 xx $!n-items; # zero everything by default
         @!data[0 .. $input.end] = |$input;
         return self;
      }
   }

   method fatal ($msg = '', :$inner = Nil) {
      FatalException.new(vm => self, :$inner, payload => $msg).throw;
   }

   method assert ($cond, $msg) { self.fatal($msg) unless $cond }

   method trace ($inst) {
      my @args = $inst<args>.map({ $_ < 32768 ?? $_ !! "r[{$_ - 32768}]" });
      my $trace = "$inst<pc-before>: $inst<name> {@args}";
      @!trace.push: $trace;
      $*ERR.say($trace) if $!verbose;
   }

   has $.memory    = AddressSpace.new(n-items => 2 ** 15, name => 'memory');
   has $.registers = AddressSpace.new(n-items => 8, name => 'registers');
   has @.stack;
   has @.trace;
   has $.pc = 0;
   has $.pc-before = 0;
   has $.last-prompt-cmd = 'p';
   has $.running = True;
   has $.run-loop = False;
   has %!breakpoint;
   has @!last-instructions;
   has $.output = '';
   has $.last-input = '';
   has @.input-sequence = [];
   has @.input = [];
   has $!verbose = False;
   has $!history;

   method md5 () {
      state $hasher = Digest::MD5.new;
      my @all-data = $!pc, |($!memory.all-values), |($!registers.all-values), |@!stack;
      $hasher.md5_hex(@all-data.join(','));
   }

   method set-history ($history) { $!history = $history }

   method to-json () {
      #my $pc-before = @!last-instructions ?? @!last-instructions[*-1]<pc-before> !! 0;
      to-json(
         {
            memory    => $!memory.data,
            registers => $!registers.data,
            :@!stack,
            :$!pc,
            :$!running,
            :$!output,
            :$!last-input,
         }
      );
   }

   method from-json ($json) {
      my $data = from-json($json);
      $.memory.init($data<memory>);
      $.registers.init($data<registers>);
      @.stack = |($data<stack>);
      $!pc = $data<pc>;
      $!running = $data<running> // True;
      $!output = $data<output>;
      $!last-input = $data<last-input> // '';
      @!input = [];
      @!trace = [];
   }

   method plain-op-for ($id) {
      state @ops =
         halt => 0,
         set  => 2,
         push => 1,
         pop  => 1,
         eq   => 3,
         gt   => 3,
         jmp  => 1,
         jt   => 2,
         jf   => 2,
         add  => 3,
         mult => 3,
         mod  => 3,
         and  => 3,
         or   => 3,
         not  => 2,
         rmem => 2,
         wmem => 2,
         call => 1,
         ret  => 0,
         out  => 1,
         in   => 1,
         noop => 0,
      ;
      return Nil unless 0 <= $id < @ops;
      return @ops[$id];
   }

   method op-for ($id) {
      my $op = self.plain-op-for($id);
      self.assert(defined($op), "invalid op for id <$id>");
      return $op;
   }

   method peek-op-id ($pc is copy = Nil) {
      $pc //= $!pc;
      my $memdata = $!memory.data;
      return $pc < $memdata.elems ?? $memdata[$pc] !! 0;
   }

   method parse-op ($pc is copy = Nil) {
      $pc //= $!pc;

      my %retval;
      %retval<id> = my $id = self.peek-op-id($pc);

      CATCH { default { self.fatal("invalid op at pc <$pc> (id<$id>)") } }
      (%retval<name>, %retval<n-args>) = self.op-for(self.peek-op-id($pc)).kv;

      return %retval;
   }

   # only parsing, no advancing of the official program counter
   method parse-instruction ($pc is rw) {
      my $data = $!memory.data;

      my $pc-before = $pc;

      my $op-data = self.parse-op($pc++);
      my $id = $op-data<id>;
      my $name = $op-data<name>;
      my $n-args = $op-data<n-args>;

      self.assert($pc + 1 + $n-args < $data.elems,
         "pc<$pc> cannot fetch $n-args arg{$n-args == 1 ?? '' !! 's'}");

      my @args = (^$n-args).map({ $data[$pc++] });

      my &method = SynacorChallengeVM.^lookup('op_' ~ $name);

      my $instruction = {
         :$pc-before,
         :$id,
         :$name,
         :@args,
         :&method,
         pc-after => $pc,
      };
      # say $instruction;
      return $instruction;
   }

   # advances the official program counter and caches the instruction
   method fetch-instruction () {
      $!pc-before = $!pc;
      my $instruction = self.parse-instruction($!pc); # pass-by reference
      @!last-instructions.push: $instruction;
      @!last-instructions.shift if @!last-instructions > 2;
      return $instruction;
   }

   # get latest cached parsed instruction, if any
   method get-last-instruction() {
      return @!last-instructions > 0  ?? @!last-instructions[*-1] !! Nil;
   }

   method register-id ($n) {
      self.assert(32768 <= $n <= 32775, "<$n> does not point to a register");
      return $n - 32768;
   }

   method get-value ($n) {
      self.assert(0 <= $n < 32776, "invalid value specification n<$n>");
      return $n if $n < 32768;
      return self.get-register($n);
   }

   method set-register ($ereg, $value) {
      self.assert(32768 <= $ereg <= 32775, "<$ereg> is not a register");
      $!registers.write($ereg - 32768, $value);
   }

   method get-register ($ereg) {
      self.assert(32768 <= $ereg <= 32775, "<$ereg> is not a register");
      $!registers.read($ereg - 32768);
   }

   method op_halt () { $!running = False; return False }
   method op_noop () { return True  }

   # stack-oriented operations
   method op_push ($a) { @!stack.push: self.get-value($a); return True }
   method op_pop ($a) {
      self.assert(@!stack > 0, 'cannot pop from empty stack');
      return self.reg-op($a, { @!stack.pop });
   }

   # flow control operations (jumps)
   method cond-jump ($cond, $target) {
      $!pc = self.get-value($target) if $cond;
      return True;
   }
   method op_jmp ($a)     { self.cond-jump(True,                    $a) }
   method op_jt  ($a, $b) { self.cond-jump(self.get-value($a) != 0, $b) }
   method op_jf  ($a, $b) { self.cond-jump(self.get-value($a) == 0, $b) }

   # subroutines support
   method op_call ($a) {
      @!stack.push: $!pc;
      return self.cond-jump(True, $a);
   }
   method op_ret {
      if @!stack == 0 {
         say 'returning with empty stack';
         return False;
      }
      return False unless @!stack;
      my $target = @!stack.pop;
      self.assert(0 <= $target < 0x8000, "out-of-range value from stack");
      return self.cond-jump(True, $target);
   }

   # register-based operations
   method reg-op ($a, &op, *@args) {
      my @values = @args.map: { self.get-value($_) };
      self.set-register($a, &op(|@values));
      return True;
   }
   method op_set  ($a, $b)     { self.reg-op($a, { $^x                    }, $b    ) }
   method op_eq   ($a, $b, $c) { self.reg-op($a, { $^x == $^y ?? 1 !! 0   }, $b, $c) }
   method op_gt   ($a, $b, $c) { self.reg-op($a, { $^x >  $^y ?? 1 !! 0   }, $b, $c) }
   method op_add  ($a, $b, $c) { self.reg-op($a, { ($^x + $^y) mod 0x8000 }, $b, $c) }
   method op_mult ($a, $b, $c) { self.reg-op($a, { ($^x * $^y) mod 0x8000 }, $b, $c) }
   method op_mod  ($a, $b, $c) { self.reg-op($a, { $^x mod $^y            }, $b, $c) }
   method op_and  ($a, $b, $c) { self.reg-op($a, { $^x +& $^y             }, $b, $c) }
   method op_or   ($a, $b, $c) { self.reg-op($a, { $^x +| $^y             }, $b, $c) }
   method op_not  ($a, $b)     { self.reg-op($a, { $^x +^ 0x7FFF          }, $b    ) }

   # memory-oriented operations
   method op_rmem ($a, $b) { self.reg-op($a, { $!memory.read($^x) }, $b) }
   method op_wmem ($a, $b) {
      $!memory.write(self.get-value($a), self.get-value($b));
      return True;
   }

   # I/O operations **UNUSED**
   method op_out ($v) {
      $*OUT.print(self.op_out_collect($v));
      return True;
   }
   method op_in ($reg) {
      my $ord = @!input.shift.ord;
      self.assert(0 <= $ord < 0x80, "read char is not ASCII (ord<$ord>)");
      return self.reg-op($reg, { $ord });
   }

   # I/O operations **REALLY USED**
   method op_out_collect ($v) {
      my $ord = self.get-value($v);
      self.assert($ord < 0x80, "invalid ASCII ord<$ord>");
      return $ord.chr;
   }

   method print-data ($pc is copy, $lines is copy = 20) {
      my @out;
      my $pc-out;
      my $data = $!memory.data;
      while $lines > 0 && $pc < $data.elems {
         my $ipc = $pc;
         my $dec = $data[$pc++];
         my $hex = $dec.base(16);
         my $str = 32 <= $dec <= 127 ?? "'{$dec.chr}'" !! '';
         put "   $ipc: $dec $hex $str";
         --$lines;
      }

      return $pc;
   }

   method print-code ($pc is copy, $lines is copy = 20, $print = True) {
      my @out;
      my $pc-out;
      my $code = $!memory.data;
      while $lines > 0 && $pc < $code.elems {
         my $ipc = $pc;
         my $id = $code[$pc++];
         return self.print-data($ipc, $lines) if $id > 21;

         my ($name, $n-args) = self.plain-op-for($id).kv;
         my @args = (^$n-args).map({ $code[$pc++] })
            .map({ $_ < 32768 ?? $_ !! "r[{$_ - 32768}]" });
         if $name eq 'out' && @args[0] ~~ Int {
            $pc-out = $ipc unless @out;
            @out.push: @args[0].chr;
         }
         else {
            if @out {
               if $print {
                  say "   $pc-out: out >";
                  my $output = @out.join('');
                  $output ~~ s:g{^^} = '   | ';
                  say $output;
               }
               else {
                  say "$pc-out: out ..."
               }
               @out = [];
               --$lines;
            }

            put "   $ipc: ", self.printable-code-line($name, @args) if $lines--;
            #say "$ipc: $name {@args}" if $lines--;
         }
      }

      return $pc;
   }

   method printable-code-line ($name, @args) {
      my ($a, $b, $c) = |@args;
      given $name {
         when any(<halt noop>) { $name }
         when 'set' { "$a = $b" }
         when any(<push pop>) { "$name $a" }
         when 'jmp' { "goto $a" }
         when 'jt'  { "goto $b if $a" }
         when 'jf'  { "goto $b unless $a" }
         when 'eq'  { "$a = $b == $c" }
         when 'gt'  { "$a = $b > $c" }
         when 'add'  { "$a = $b + $c" }
         when 'mult'  { "$a = $b * $c" }
         when 'mod'  { "$a = $b mod $c" }
         when 'and'  { "$a = $b & $c" }
         when 'or'  { "$a = $b | $c" }
         when 'not'  { "$a = ¬$b" }
         when 'rmem'  { "$a = Mem[$b]" }
         when 'wmem'  { "Mem[$a] = $b" }
         when 'call'  { "gosub $a" }
         when 'ret'   { 'return' }
         when 'in'   { "$a = read(IN)" }
         default { "$name {@args}" }
      }
   }

   method dump-registers {
      say 'registers: ', "pc[$!pc] ", $!registers.all-values.kv.map({ $^i ~ "[$^v]" }).join(' ');
   }

   method dump-stack {
      my @stack = |@!stack;
      @stack = '[...]', |(@!stack[*-7 .. *-1]) if @!stack > 7;
      @stack = '(empty)' unless @stack;
      say 'stack: ', @stack.join(', ');
   }

   method dump {
      self.dump-registers;
      self.dump-stack;
      self.print-code($!pc, 5, False);
   }

   method step-generic ($instruction) {
      self.trace($instruction);
      my &method = $instruction<method>;
      return self.&method(|($instruction<args>));
   }

   method step-output ($instruction is copy) {
      my $pc = $instruction<pc-before>; # where the out starts from
      my $output = '';
      my \out-instruction-id = 19; # FIXME Magic Number
      loop {
         self.trace($instruction);
         $output ~= self.op_out_collect(|($instruction<args>));
         last if self.peek-op-id != out-instruction-id;
         $instruction = self.fetch-instruction;
      }

      $*OUT.print($output) unless $!verbose;
      $!output ~= $output;

      return True; # out is True
   }


   method load-history ($filename = Nil) {
      $!history = SynacorChallengeVMHistory(savefile => $filename.Str)
         if defined $filename;
      $!history.load;
   }

   method collect-input () {
      INPUT:
      loop {
         my $input;
         if @!input-sequence {
            $input = @!input-sequence.shift;
            say "auto> $input";
         }
         else {
            $input = prompt("> ");
         }
         given $input {
            when ! defined($_) { return False }
            when '/break' { $!run-loop = False; return False }
            when m{^ \/ (\/.*) } {
               $!last-input = $/[0].Str;
               last INPUT;
            }
            when m{^ \/ (.*) }  {
               self.react($/[0].Str);
            }
            default {
               $!last-input = $input;
               last INPUT;
            }
         }
      }
      @!input = ($!last-input ~ "\n").comb;
      return True;
   }

   method dwim-step () {
      my $next-op = self.parse-op;

      # trap input to collect stuff
      if $next-op<name> eq 'in' && ! @!input {
         $!history.push(self);
         return False unless self.collect-input;
      }

      my $instruction = self.fetch-instruction;
      given $instruction<name> {
         when 'out' { return self.step-output($instruction)  }
         default    { return self.step-generic($instruction) }
      }
   }

   method dump-output () {
      say '-' x 50;
      say $!output;
      say '-' x 50;
   }

   method reset-to (@code) {
      $!memory.init(@code);
      $!registers.init; # zeroes everything by default
      @!stack = [];
      @!trace = [];
      $!pc = $!pc-before = 0;
      @!last-instructions = [];
      @!input = [];
      return self;
   }

   method react ($cmd is copy) {
      $cmd //= 'q';
      $cmd = $!last-prompt-cmd unless $cmd.chars;
      $!last-prompt-cmd = $cmd;
      my $outcome = True;
      state $current-room;
      state $last-move;
      state %graph;
      state %id-of;
      given $cmd {
         when 'q' { $outcome = False }
         when /^ back \s* (\d+)? $/ {
            my $previous-to-last = $!history.end - 1;
            $previous-to-last = 0 if $previous-to-last < 0;
            my $point = ($/[0] // ($!history.end - 1)).Int;
            $point = 0 if $point < 0;
            $!history.pop while $point != $!history.end;
            $!history.top(self);
         }
         when /^ b \s* (\d+)?/ {
            my $bp = $/[0];
            if defined($bp) {
               %!breakpoint{$bp.Str} = 1;
               say "set breakpoint at {$/[0].Str}";
            }
            else {
               say 'breakpoints: ', %!breakpoint.keys.sort({$^a <=> $^b}).join(', ');
            }
         }
         when /^ B \s* (\d+)/ {
            %!breakpoint{$/[0].Str}:delete;
            say "removed breakpoint at {$/[0].Str}";
         }
         when 'd' { self.dump }
         when 'h' {
            my @inputs = $!history.data-map({ '<' ~ ($^r<last-input> // '(-)') ~ '>'});
            .join('. ').put for @inputs.kv.map({[$^k, $^v]});
         }
         when 'hash' { say self.md5 }
         when /^ j \s* (\d+)/ {
            self.cond-jump(True, $/[0].Int);
            self.dump;
         }
         when /^ load [\s+ (\S+)]?/ { self.load-history($/[0]) }
         when /^ l [\s* (.*)?]?/ {
            my $filename = ($/[0] // '').Str;
            $filename = 'synacor.save' unless $filename.chars;
            self.from-json($filename.IO.slurp);
            say 'run will restart from last executed instruction, included';
         }
         when 'o' { self.dump-output }
         when 'pop' { $!history.pop(self) }
         when /^ p \s* (\d+)? $/ {
            my $next-pc = self.print-code(($/[0] // $!pc).Int);
            $!last-prompt-cmd = "p $next-pc";
         }
         when /^ P \s* (\d+)? $/ {
            my $next-pc = self.print-data(($/[0] // $!pc).Int);
            $!last-prompt-cmd = "P $next-pc";
         }
         when 'r' { self.dump-registers }
         when /^ r8 \s+ (\d+)/ {
            $!registers.write(7, $/[0].Int);
         }
         when /^ restore \s+ (\d+) \s* (.*)/ {
            my $point = $/[0].Int;
            if $point < $!history.elems {
               $!history.restore(self, $point);
               my $last = $!last-input ~ '';
               $!last-input = ($/[1] // '').Str;
               $!last-input = "restored <$point> $last" unless $!last-input.chars;
               $!history.push(self);
            }
         }
         when 'room' {
            @!input-sequence = 'this is just a bunch of gibberish', 'look', '/tag';
         }
         when /^ move \s+ (.+)/ {
            $last-move = $/[0].Str;
            @!input-sequence = $last-move, 'this is just a bunch of gibberish', 'look', '/tag';
         }
         when 'run' {
            $!run-loop = True;
            while $!running && $!run-loop {
               if %!breakpoint{$!pc}:exists {
                  say '** BREAKPOINT **';
                  $!run-loop = False;
               }
               else {
                  $!run-loop = self.dwim-step;
               }
            }
            say '';
            self.dump;
         }
         when 's' {
            if $!running {
               $!running = self.dwim-step;
               say '';
               self.dump;
            }
            else { say 'not running any more' }
         }
         when /^ save [\s+ (\S+)]?/ {
            my $filename = $/[0];
            $!history.savefile = $filename.Str if defined $filename;
            if defined $!history.savefile { $!history.save }
            else { say 'no savefile set' }
         }
         when 'snap' { $!history.push(self) }
         when 'map' {
            for %id-of.values.sort({ $^a <=> $^b }) -> $room {
               put "room [$room]:";
               for %graph{$room}.kv -> $move, $adjacent-room {
                  put "   $move -> [$adjacent-room]";
               }
            }
         }
         when 'tag' {
            my $hash = self.md5;
            if %id-of{$hash}:exists {
               say "room {%id-of{$hash}} (already visited)";
            }
            else {
               %id-of{$hash} = %id-of.elems;
               say "room {%id-of{$hash}} (newly visited)";
            }
            my $id = %id-of{$hash};
            if defined($current-room) && defined($last-move) {
               if %graph{$current-room}{$last-move}:exists {
                  die "inconsistent maze" if %graph{$current-room}{$last-move} != $id;
               }
               else {
                  %graph{$current-room}{$last-move} = $id;
               }
            }
            $last-move = Nil;
            $current-room = $id;
         }
         when 'top' { $!history.restore(self) }
         when /^ t \s* (\d+)? $/ {
            my $amount = $/[0] || 10;
            $amount = @!trace.elems if $amount > @!trace.elems;
            say '-' x 50;
            .say for @!trace[*-$amount .. *-1];
            say '';
            self.dump;
         }
         when 'v' { $!verbose = True }
         when 'V' { $!verbose = False }
         when /^ x \s* (\d+) [ \s+ (\d+)?]?/ {
            my $address = $/[0].Int;
            my $amount = ($/[1] // 1).Int;
            for ^$amount {
               my $value = $!memory.read($address);
               say "$address: $value";
               ++$address;
            }
         }
         when ''  {  }
         default { say "cannot understand command" }
      }
      return $outcome;
   }

   method help() {
      put q:to/END/;

b           list breakpoints
b <n>       set breakpoint at n
B <n>       remove breakpoint at n
back [<n>]  go back to saved state n (previous one by default)
d           dump (FIXME meaning?!?)
h           print current history
hash        print MD5 of current state
l <file>    load state from file (FIXME obsolete with load below?)
load <file> load history from file
o           dump output (FIXME meaning?!?)
p [<a>]     print code (from address or from pc or continuing...)
P [<a>]     print data (from address or from pc or continuing...)
pop         remove last history and use it to restore as current state
r           print registers
r8 <n>      set value for register 8
restore <n> [<rest>]
top         restore last history as current state
q           quit


END
   }

   method run () {
      CATCH {
         # this will do some adaptation from inner exceptions
         when SynacorChallengeVM::FatalException { .throw }
         default { self.fatal(inner => $_) }

         # this is where we really say something
         #   CATCH { default { $*ERR.say: .message } }
      }

      $!history //= SynacorChallengeVMHistory.new;
      $!last-prompt-cmd = 'p';
      @!trace = [];
      COMMAND:
      loop {
         my $prompt = $!running ?? "[$!last-prompt-cmd] " !! '(!) ';
         $prompt = '(v) ' ~ $prompt if $!verbose;
         my $cmd = prompt($prompt) // last;
         last unless self.react($cmd);
      }

      return;
   }
}

class SynacorChallengeVMHistory {
   has @!snapshots;
   has $.savefile is rw is built;

   method load ($filename = $!savefile) {
      my $blob = $filename.IO.slurp(:bin);
      $blob = decompressToBlob($blob) if $blob[0] == 'B'.ord;
      my $data = from-json($blob.decode);
      @!snapshots = |($data<snapshots>);
      return self;
   }

   method save ($filename = $!savefile) {
      my $hash = hash(snapshots => @!snapshots);
      my $json = to-json($hash);
      my $compressed = compressToBlob(Buf[uint8].new($json.encode));
      $filename.IO.spurt($compressed);
      return self;
   }


   method end   () { @!snapshots.end    }
   method elems () { @!snapshots.elems  }

   method push (SynacorChallengeVM $vm) {
      @!snapshots.push: $vm.to-json;
   }

   method !restore-to ($vm, $snapshot) { $vm.from-json($snapshot); $vm }
   multi method pop ()       { @!snapshots.pop; return Nil }
   multi method pop ($spawn where * === True) { self.pop(SynacorChallengeVM.new) }
   multi method pop (SynacorChallengeVM $vm) {
      die 'no snapshot available' unless @!snapshots;
      return self!restore-to($vm, @!snapshots.pop);
   }
   multi method top () { self.top(SynacorChallengeVM.new) }
   multi method top (SynacorChallengeVM $vm) {
      die 'no snapshot available' unless @!snapshots;
      return self!restore-to($vm, @!snapshots[*-1]);
   }
   multi method restore (SynacorChallengeVM $vm, Int $i where * >= 0) {
      die "not enough saved states ($i)" if $i > @!snapshots.end;
      return self!restore-to($vm, @!snapshots[$i]);
   }
   multi method restore (SynacorChallengeVM $vm) { self.top($vm) }
   method spawn ($i) { self.restore(SynacorChallengeVM.new, $i)}

   method data-map (&cb) { @!snapshots.map: { &cb(from-json($_)) } }
}

class Digest::MD5:auth<cosimo>:ver<0.05> {
    sub prefix:<¬>(\x)       {   (+^ x) % 2**32 }
    sub infix:<⊞>(\x, \y)    {  (x + y) % 2**32 }
    sub infix:«<<<»(\x, \n)  { (x +< n) % 2**32 +| (x +> (32-n)) }

    my \FGHI = -> \X, \Y, \Z { (X +& Y) +| (¬X +& Z) },
               -> \X, \Y, \Z { (X +& Z) +| (Y +& ¬Z) },
               -> \X, \Y, \Z { X +^ Y +^ Z           },
               -> \X, \Y, \Z { Y +^ (X +| ¬Z)        };

    my \S = (
            (7, 12, 17, 22) xx 4,
            (5,  9, 14, 20) xx 4,
            (4, 11, 16, 23) xx 4,
            (6, 10, 15, 21) xx 4,
        ).flat;


    my \T = (floor(abs(sin($_ + 1)) * 2**32) for ^64).flat;

    my \k = (
            (   $_           for ^16),
            ((5*$_ + 1) % 16 for ^16),
            ((3*$_ + 5) % 16 for ^16),
            ((7*$_    ) % 16 for ^16),
        ).flat;


    sub little-endian($w, $n, *@v) { (@v X+> flat ($w X* ^$n)) X% (2 ** $w) }

    sub md5-pad($msg) {
        my \bits = 8 * $msg.elems;
        my @padded = flat $msg.list, 0x80, 0x00 xx (-(bits div 8 + 1 + 8) % 64);
        flat @padded.map({ :256[$^d,$^c,$^b,$^a] }), little-endian(32, 2, bits);
    }

    sub md5-block(@H, @X) {
        my ($A, $B, $C, $D) = @H;
        for ^64 -> \i {
            my \f = FGHI[i div 16]($B, $C, $D);
              ($A, $B,                                         $C, $D)
            = ($D, $B ⊞ (($A ⊞ f ⊞ T[i] ⊞ @X[k[i]]) <<< S[i]), $B, $C);
        }
        @H «⊞=» ($A, $B, $C, $D);
    }

    our sub md5($msg) {
        my @M = md5-pad($msg);
        my @H = 0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476;
        md5-block(@H, @M[$_ .. $_+15]) for 0, 16 ...^ +@M;
        Buf.new: little-endian(8, 4, @H)
    }

    multi method md5_hex(Str $str) {
        md5( $str.encode('latin-1') ).list».fmt('%02x').join
    }

    multi method md5_hex(@str) {
        md5( @str.join.encode('latin-1') ).list».fmt('%02x').join
    }

    multi method md5_buf(Str $str --> Buf) {
        md5( $str.encode('latin-1') );
    }

    multi method md5_buf(@str --> Buf) {
        md5( @str.join.encode('latin-1') );
    }
}
