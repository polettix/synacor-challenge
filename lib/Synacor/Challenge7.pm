package Synacor::Challenge7;
use v5.24;
use Carp;
use English qw< -no_match_vars >;
use experimental qw< signatures >;

use Exporter qw< import >;
our @EXPORT_OK = qw< ackermann_with_options >;

use constant MODULUS => 0x8000;
use constant TARGET  => 6;

sub ackermann_with_options ($m, $n, $reset, $mod = MODULUS) {
   my %cache;
   my @stack = ([], [$m, $n]);
   my $depth = 2;
   while (@stack > 1) {
      $depth = @stack if @stack > $depth;
      my ($m, $n, @vs) = $stack[-1]->@*;
      if (exists($cache{$m}{$n})) {
         pop @stack;
         push $stack[-1]->@*, $cache{$m}{$n};
      }
      elsif ($m == 0) {
         $cache{$m}{$n} = ($n + 1) % $mod;
      }
      else {
         my $m_1 = $m == 0 ? $mod - 1 : $m - 1;
         if ($n == 0) {
            if (@vs == 1) {
               $cache{$m}{$n} = $vs[0];
            }
            else {
               push @stack, [$m_1, $reset];
            }
         }
         else {
            if (@vs == 2) {
               $cache{$m}{$n} = $vs[1];
            }
            elsif (@vs == 1) {
               push @stack, [ $m_1, $vs[0] ];
            }
            else {
               my $n_1 = $n == 0 ? $mod - 1 : $n - 1;
               push @stack, [ $m, $n_1 ];
            }
         }
      }
   }
   #warn "max depth: $depth\n";
   return $stack[0][0];
}

# modulino!
if (! caller()) {
   $|++;
   my ($from, $to) = @ARGV;
   $from //= 1;
   $to   //= MODULUS - 1;
   for my $reset ($from .. $to ) {
      my $value = ackermann_with_options(4, 1, $reset);
      say {*STDERR } "$reset --> $value";
      next if $value != TARGET;
      say "found: $reset";
      last;
   }
}

1;
