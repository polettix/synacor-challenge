package Synacor::Architecture;
use v5.24;
use experimental qw< signatures >;
use Exporter qw< import >;

our @EXPORT_OK = qw<
   OPS
   OP_IN OP_OUT OP_NOOP
>;
our %EXPORT_TAGS = (
   all => \@EXPORT_OK,
   constants => [qw< OPS OP_OUT OP_IN OP_NOOP >],
);

use constant OPS => [
   { name => 'halt', args => 0 },
   { name => 'set',  args => 2 },
   { name => 'push', args => 1 },
   { name => 'pop',  args => 1 },
   { name => 'eq',   args => 3 },
   { name => 'gt',   args => 3 },
   { name => 'jmp',  args => 1 },
   { name => 'jt',   args => 2 },
   { name => 'jf',   args => 2 },
   { name => 'add',  args => 3 },
   { name => 'mult', args => 3 },
   { name => 'mod',  args => 3 },
   { name => 'and',  args => 3 },
   { name => 'or',   args => 3 },
   { name => 'not',  args => 2 },
   { name => 'rmem', args => 2 },
   { name => 'wmem', args => 2 },
   { name => 'call', args => 1 },
   { name => 'ret',  args => 0 },
   { name => 'out',  args => 1 },
   { name => 'in',   args => 1 },
   { name => 'noop', args => 0 },
];

use constant OP_OUT  => 19;
use constant OP_IN   => 20;
use constant OP_NOOP => 21;

1;
