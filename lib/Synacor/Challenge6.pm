package Synacor::Challenge6;
use v5.24;
use Carp;
use English qw< -no_match_vars >;
use experimental qw< signatures >;

use constant TARGET => 399;
use constant VALUES => [2, 3, 5, 7 , 9];
sub function { $_[0] + $_[1] * $_[2] ** 2 + $_[3] ** 3 - $_[4] }

if (! caller()) {
   my @solution = challenge6_solver(TARGET, \&function, VALUES->@*);
   say "solution (@solution)";
}

sub challenge6_solver ($target, $function, @values) {
   my $it = permutations_iterator(items => \@values);
   while (my @permutation = $it->()) {
      my $value = $function->(@permutation);
      return @permutation if $value == $target;
   }
   return;
}

sub permutations_iterator {
   my %args = (@_ && ref($_[0])) ? %{$_[0]} : @_;
   my $items = $args{items} || die "invalid or missing parameter 'items'";
   my $filter = $args{filter} || sub { wantarray ? @_ : [@_] };
   my @indexes = 0 .. $#$items;
   my @stack = (0) x @indexes;
   my $sp = undef;
   return sub {
      if (! defined $sp) { $sp = 0 }
      else {
         while ($sp < @indexes) {
            if ($stack[$sp] < $sp) {
               my $other = $sp % 2 ? $stack[$sp] : 0;
               @indexes[$sp, $other] = @indexes[$other, $sp];
               $stack[$sp]++;
               $sp = 0;
               last;
            }
            else {
               $stack[$sp++] = 0;
            }
         }
      }
      return $filter->(@{$items}[@indexes]) if $sp < @indexes;
      return;
   }
}


1;
