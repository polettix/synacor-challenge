package Synacor::Dbg;
use v5.24;
use Moo;
use experimental qw< signatures >;
use Path::Tiny;
use Synacor::Architecture qw< :constants >;
use Synacor::VM;
use Cpanel::JSON::XS qw< decode_json encode_json >;
use Term::ReadLine;
use Try::Catch;


use constant RUN_STEP_DONE  => 0;
use constant RUN_NO_INPUT   => 1;
use constant RUN_BREAKPOINT => 2;
use constant RUN_RETURN     => 3;
use constant RUN_HALT => 4;

sub represent_value ($x) {
   return $x if $x < 0x8000;
   return 'r' . ($x - 0x8000) if $x < 0x8008;
   return '**invalid**';
}

sub _coerce_breakpoints ($bps) {
   return $bps if ref($bps) eq 'HASH';
   return { map { $_ => 1 } $bps->@* };
}

sub trimsplit ($str) { grep { length } split m{\s+}mxs, ($str // '') }

use constant VM_METHODS =>
   [qw< call_stack is_running memory pc registers stack >];

use namespace::clean;

has program_file => (is => 'rw', default => undef);
has debug_file   => (is => 'rw', default => undef);
has vm   => (is => 'lazy', handles => VM_METHODS);
has address_for => (is => 'rw', default => sub { return {} });
has label_at => (is => 'rw', default => sub { return {} });
has breakpoints => (is => 'rw', default => sub { return {} },
   coerce => \&_coerce_breakpoints);
has deasm_pc => (is => 'rw');
has deasm_length => (is => 'rw', default => 20);
has sequence_for => (is => 'rw', default => sub { return {} });
has term => (is => 'lazy', clearer => 1);
has auto_dump => (is => 'rw', default => 1);
has auto_print => (is => 'rw', default => 1);
has room_address => (is => 'rw', default => undef); # Aneurysm9 => 2754, polettix => 2732
has input_mode => (is => 'rw', default => 0);

sub _build_term ($self) { return Term::ReadLine->new('sc') }

sub _build_deasm_pc ($self) { $self->pc }

sub _load_debug_file ($self, $file, $vm) {
   my $data = decode_json(path($file)->slurp_raw);

   $vm->$_(delete($data->{$_})) for VM_METHODS->@*;
   $self->term->SetHistory((delete($data->{history}) // [])->@*);

   for my $key (keys($data->%*)) {
      if (my $method = $self->can($key)) {
         $self->$method(delete($data->{$key}));
      }
      else {
         stderr("ignoring key <$key> while loading debug file");
      }
   }

   return $self;
}

sub _build_vm ($self) {
   my $instance = Synacor::VM->new;

   if (-e (my $dfile = $self->debug_file // '')) {
      $self->_load_debug_file($dfile, $instance);
   }
   elsif (-e (my $pfile = $self->program_file // '')) {
      $instance->load_binary($pfile);
   }

   return $instance;
}

sub load_debug_file ($self, $filename = undef) {
   $filename //= $self->debug_file;
   return $self->_load_debug_file($filename, $self->vm);
}

sub save_debug_file ($self, $filename = undef) {
   $filename //= $self->debug_file;
   my $vm = $self->vm;
   my $saver = {
      is_running  => $vm->is_running,
      memory      => $vm->memory,
      pc          => $vm->pc,
      registers   => $vm->registers,
      stack       => $vm->stack,
      call_stack  => $vm->call_stack,
      label_at    => $self->label_at,
      address_for => $self->address_for,
      breakpoints => [ sort { $a <=> $b } keys($self->breakpoints->%*) ],
      history => [ $self->term->GetHistory ],

      # game-specific stuff
      room_address => $self->room_address,
   };
   path($filename)->spew_raw(encode_json($saver));
   return $self;
}

sub _cmd_aliases ($self) {
   my %alias_for = (
      r => 'run',
      ra => 'room_address',
      s => 'step',
      so => 'step_over',
      ret => 'return',
      p => 'print',
      d => 'dump',
      q => 'quit',
      i => 'input',
      h => 'history',
      b => 'breakpoint',
      break => 'breakpoint',
      '//' => 'input_mode',
   );

   return \%alias_for;
}

sub cmd_room_address ($self, $arg) {
   if (length($arg // '')) { # set
      $self->room_address($arg);
   }
   elsif (defined(my $ra = $self->room_address)) {
      stderr('room address set at: ', $self->room_address);
   }
   else {
      stderr('room address not set yet');
   }
}

sub printable_room ($self) {
   my $ra = $self->room_address // 'nope';
   return unless $ra =~ m{\A \d+ \z}mxs;
   my $memory = $self->memory;
   my $ra1 = $memory->[$self->room_address];
   my $na = $memory->[$ra1];
   my $len = $memory->[$na];
   my $name = join '', map { chr($_) } $memory->@[($na+1)..($na+$len)];
   return "room id<$ra1> name<$name>";
}

sub prompt ($self) {
   my @parts = "\n";
   push @parts, $self->printable_dump . "\n" if $self->auto_dump;

   if (defined(my $pr = $self->printable_room)) {
      push @parts, $pr . "\n";
   }

   push @parts, $self->input_mode ? '(I)'
      : $self->vm->input eq '' ? '(!i)' : '(i)';
   my $prompt = join('', @parts);
   return "$prompt> ";
}

sub start ($self) {
   state $alias_for = $self->_cmd_aliases;

   $self->vm->is_running(1);
   my $term = $self->term;
   my $last = '';
   while (defined(my $input = $term->readline($self->prompt))) {
      $input =~ s{\A\s+|\s+\z}{}gmxs;
      $input = $last unless length($input);
      next unless length($input);
      $last = $input;

      if ($self->input_mode) {
         if ($input eq '//') {
            $self->input_mode(0);
            next;
         }
         if (substr($input, 0, 1) eq '/') {
            $input = substr($input, 1);
         }
         else {
            $input = "run $input";
         }
      }

      my ($cmd_name, $arg) = split m{\s+}mxs, $input, 2;
      my $method_name = lc($cmd_name =~ s{-}{_}rgmxs);
      $method_name = $alias_for->{$method_name} // $method_name;
      last if $method_name eq 'quit';
      try {
         if (my $method = $self->can('cmd_' . $method_name)) {
            $self->$method($arg);
         }
         else {
            stderr("unsupported command '$cmd_name'");
         }
      }
      catch {
         my $exception = ref($_) ? $$_ : $_;
         stderr("exception: $exception");
      };
   }
   stderr('bye');
   return $self;
}

sub cmd_input_mode ($self, $arg) { $self->input_mode($arg //= '1') }

sub emit ($fh, $prefix, @args) {
   my $printable = join('', @args);
   $printable =~ s{^}{$prefix}gmxs if length($prefix);
   say {$fh} $printable;
}

sub stderr (@args) { emit(\*STDERR, '-- ', @args) }
sub stdout (@args) { emit(\*STDOUT, '', @args) }

sub cmd_savefile ($self, $arg) {
   if (length($arg // '')) {
      $self->debug_file($arg);
   }
   else {
      stderr($self->debug_file // '**undef**');
   }
   return $self;
}

sub printable_dump ($self) {
   my @overview;
   push @overview,  join('', 'pc[', $self->pc, ']');
   my $registers = $self->registers;
   push @overview, map {
      'r' . $_ . '[' . $registers->[$_] . ']'
   } 0 .. 7;
   return join(' ', @overview);
}

sub cmd_dump ($self, $arg) { stderr($self->printable_dump) }

sub cmd_breakpoint ($self, $arg) {
   my $bps = $self->breakpoints;
   if (length($arg // '')) {
      for my $item (split m{\s+}mxs, $arg) {
         next unless length($item // '');
         my ($del, $addr) = $item =~ m{\A ([!-]?) (.*) \z}mxs;
         if (defined($addr = $self->resolve_address($addr))) {
            delete($bps->{$addr});
            $bps->{$addr} = 1 unless $del;
         }
         else {
            stderr("invalid breakpoint specification '$item'");
         }
      }
   }
   else {
      stderr("break $_") for sort { $a <=> $b } keys($bps->%*);
   }
   return;
}

sub cmd_peek ($self, $arg) {
   my $memory = $self->vm->memory;
   for my $item (split m{\s+}mxs, $arg) {
      next unless length($item // '');
#      my ($address, $n) = $item =~ m{
#         \A(0 | [1-9]\d*) (?: : (0 | [1-9]\d*) )? \z}mxs;
      my ($address, $n) = split m{:}, $item, 2;
      if (my ($reg) = $address =~ m{\A r([0-7]) \z}imxs) {
         $address = $self->registers->[$reg];
      }
      else {
         $address = $self->resolve_address($address);
      }
      if (! defined($address)) {
         stderr("invalid specification '$item'");
         next;
      }
      elsif ($address >= 0x8000) {
         stderr("invalid address $address from '$item'");
         next;
      }
      $n = 1 if length($n // '') == 0;
      if ($n !~ m{\A [1-9]\d* \z}mxs) {
         stderr("invalid length $n from '$item'");
         next;
      }
      for (1 .. $n) {
         my $value = $memory->[$address];
         $value .= " '" . chr($value) . "'"
            if 0x20 <= $value && $value < 0x80;
         stderr("$address -> $value");
         ++$address;
         last if $address >= 0x8000;
      }
   }
}

sub cmd_xor_peek ($self, $args) {
   my $memory = $self->vm->memory;
   for my $item (trimsplit($args)) {
      my ($addr, $xv) = split m{:}mxs, $item;
      my $address;
      if (my ($reg) = $addr =~ m{\A r([0-7]) \z}imxs) {
         $address = $self->registers->[$reg];
      }
      else {
         $address = $self->resolve_address($addr);
      }
      if (! defined($address)) {
         stderr("invalid specification '$item'");
         next;
      }
      elsif ($address >= 0x8000) {
         stderr("invalid address $address from '$item'");
         next;
      }

      my $text = '[';
      my $n = $memory->[$address++] + 1;
      while (--$n > 0) {
         my $value = $memory->[$address++] ^ $xv;
         if ((0x20 <= $value && $value < 0x80) || $value == 0x0a){
            $text .= chr($value);
         }
         else {
            $text .= "<$value>";
         }
      }
      $text .= ']';

      stderr($text);
   }
}

sub cmd_jmp ($self, $args) {
   $self->vm->pc($args);
}

sub cmd_text_peek ($self, $args) {
   my $memory = $self->vm->memory;
   for my $item (trimsplit($args)) {
      my $address;
      if (my ($reg) = $item =~ m{\A r([0-7]) \z}imxs) {
         $address = $self->registers->[$reg];
      }
      else {
         $address = $self->resolve_address($item);
      }
      if (! defined($address)) {
         stderr("invalid specification '$item'");
         next;
      }
      elsif ($address >= 0x8000) {
         stderr("invalid address $address from '$item'");
         next;
      }

      my $text = '';
      my $n = $memory->[$address++] + 1;
      while (--$n > 0) {
         my $value = $memory->[$address++];
         if (0x20 <= $value && $value < 0x80) {
            $text .= chr($value);
         }
         else {
            $text .= "\n<<<truncated, $n not represented>>>";
            last;
         }
      }

      stderr($text);
   }
}

sub cmd_save ($self, $arg) {
   $self->save_debug_file($arg);
}

sub cmd_load ($self, $arg) {
   $self->load_debug_file($arg);
}

sub cmd_history ($self, $arg) {
   stderr($_) for $self->term->GetHistory;
}

sub cmd_calls ($self, $arg) {
   my @cs = $self->vm->call_stack->@*;
   splice(@cs, 0, 2) unless $arg; # these are from the initial test...
   for my $frame (@cs) {
      my ($from, $target) = $frame->@*;
      my $ret = $from + 2;
      stderr("from=$from to=$target ret=$ret");
   }
}

sub resolve_address ($self, $address) {
   return $address unless defined $address;
   return $address if $address =~ m{\A \d }mxs;
   my $af = $self->address_for;
   return $af->{$address} // undef;
}

sub cmd_pp ($self, $arg) {
   $self->cmd_print('pc');
}

sub cmd_print ($self, $arg) {
   ($arg //= '') =~ s{\A\s+|\s+\z}{}gmxs;
   if ($arg eq '*' || $arg eq 'pc') {
      $self->deasm_pc($self->pc);
   }
   elsif (length($arg)) {
      my ($address, $len) = split m{:}mxs, $arg;
      $address = $self->resolve_address($address);
      if (! defined($address)) {
         stderr("invalid address specification '$arg'");
         return;
      }
      $self->deasm_pc($address);
      $self->deasm_length($len) if length($len // '');
   }
   my @rows = $self->deasm;
   stderr($_) for @rows;
}

sub printout_run_retval ($self, $retval) {
   if ($retval == RUN_NO_INPUT) {
      #stderr('input needed');
   }
   elsif ($retval == RUN_BREAKPOINT) {
      stderr('breakpoint hit');
   }
   elsif ($retval == RUN_HALT) {
      stderr('halted');
   }
   else {} # do not print anything
}

sub cmd_run ($self, $arg) {
   $self->cmd_input($arg) if length($arg // '');
   $self->printout_run_retval($self->run);
}

sub chatty_step ($self, $n, $message, $prefix) {
   my $vm = $self->vm;

   my $pc = $vm->pc;
   $self->decorated_deasm($n, $message, $prefix) if $n > 0;

   my $op = $vm->current_op;
   my $pc_nojump = $pc + 1 + $op->{args};

   $self->printout_run_retval($self->step);

   return $vm->pc != $pc_nojump;
}

sub decorated_deasm ($self, $n, $message, $prefix) {
   stderr($message) if defined($message);
   stderr(s{^}{$prefix}rgmxs) for  $self->deasm($self->pc, $n);
   return;
}

sub cmd_step ($self, $arg) {
   my $n = ($arg // '') =~ m{\A\d+\z}mxs ? $arg : 1;
   my $vm = $self->vm;
   my $pp_pc = $vm->pc;
   for my $step (1 .. $n) {
      my $pc = $vm->pc;
      my $op = $vm->current_op;
      my $pc_nojump = $pc + 1 + $op->{args};
      $self->printout_run_retval($self->step);
      my $vm_pc = $vm->pc;
      $pp_pc = $vm_pc == $pc_nojump ? $pc : $vm_pc;
   }

   if ($self->auto_print) {
      stderr($_) for $self->deasm($pp_pc, 3);
   }
}

sub cmd_step_over ($self, $arg) {
   $self->printout_run_retval($self->step_over);
   if ($self->auto_print) {
      stderr($_) for $self->deasm($self->pc, 3);
   }
}

sub cmd_label ($self, $arg) {
   my $af = $self->address_for;
   my $lf = $self->label_at;
   my $n = 0;
   for my $spec (trimsplit($arg)) {
      ++$n;
      if (substr($spec, 0, 1) eq '!') {
         my $label = substr($spec, 1);
         if (exists($af->{$label})) {
            my $previous_address = delete($af->{$label});
            my $as = $lf->{$previous_address};
            $as->@* = grep { $_ ne $label } $as->@*;
         }
         else {
            stderr("no such label: '$label'");
         }
         next;
      }

      my ($label, $address) = split m{[:=]}mxs, $spec;
      $address //= $self->pc;
      ($address, $label) = ($label, $address)
         if $address !~ m{\A (?: 0 | [1-9]\d*) \z}mxs;
      if ($address !~ m{\A (?: 0 | [1-9]\d*) \z}mxs) {
         stderr("invalid specification '$spec': no address");
         next;
      }
      if ($label =~ m{\A \d }mxs) {
         stderr("invalid specification '$spec': label cannot start with digit");
         next;
      }

      # ok, we have a label and an address. One label can only point to
      # one address; one address might go back to multiple labels though
      if (exists($af->{$label})) {
         my $previous_address = delete($af->{$label});
         my $as = $lf->{$previous_address};
         $as->@* = grep { $_ ne $label } $as->@*;
      }

      $af->{$label} = $address;
      push(($lf->{$address} //= [])->@*, $label);
   }

   if (! $n) {
      stderr("$_ -> $af->{$_}") for sort keys($af->%*);
   }
}

sub cmd_poke ($self, $args) {
   for my $spec (trimsplit($args)) {
      my ($dest, $values) = split m{[:=]}mxs, $spec, 2;
      for my $value (split m{,}mxs, $values) {
         if ($value !~ m{\A (?:0 | [1-9]\d*) \z}mxs) {
            stderr("invalid spec '$spec'");
            next;
         }
         if ($value >= 0x8008) {
            stderr("invalid value in '$spec'");
            next;
         }
         if (my ($reg) = $dest =~ m{\A r ([0-7]) \z}imxs) {
            $self->registers->[$reg] = $value;
         }
         else {
            $dest = $self->resolve_address($dest);
            if (! defined($dest)) {
               stderr("invalid address in '$spec'");
               next;
            }
            $self->memory->[$dest++] = $value;
         }
      }
   }
}

sub cmd_input ($self, $arg) {
   my $vm = $self->vm;
   my $input = $vm->input;
   if (length($arg // '')) {
      my $new_input =
         join "\n", 
         grep { length }
         map { s{\A\s+|\s+\z}{}rgmxs }
         split m{,}mxs, $arg;
      $vm->input($input . $new_input . "\n") if length($new_input);
   }
   elsif (length($input)) {
      $input =~ s{^}{  < }gmxs;
      stderr("input:\n$input");
   }
   else {
      stderr("no input");
   }
}

# use constant RUN_STEP_DONE  => 0;
# use constant RUN_NO_INPUT   => 1;
# use constant RUN_BREAKPOINT => 2;
# use constant RUN_RETURN     => 3;
# use constant RUN_HALT => 4;

sub step ($self) {
   my $vm = $self->vm;
   my $pc = $vm->pc;
   return RUN_NO_INPUT
      if $vm->memory->[$pc] == OP_IN && length($vm->input) == 0;
   $vm->step;
   return $vm->is_running ? RUN_STEP_DONE : RUN_HALT;
}

sub step_over ($self) {
   my $op = $self->vm->current_op;
   my $step_retval = $self->step;
   return $step_retval
      if $step_retval != RUN_STEP_DONE || $op->{name} ne 'call';
   return $self->step_out;
}

sub step_out ($self, $levels = 1) {
   my $current_depth = $self->vm->call_stack->@*;
   return $self->run($current_depth - $levels);
}

sub run ($self, $return_depth = -1) {
   my $vm = $self->vm;
   my $memory = $vm->memory;
   my $call_stack = $vm->call_stack;
   my $breakpoints = $self->breakpoints;
   while ($call_stack->@* > $return_depth) {
      my $step_retval = $self->step;
      return $step_retval if $step_retval != RUN_STEP_DONE;
      return RUN_BREAKPOINT if $breakpoints->{$vm->pc};
   }
   return RUN_RETURN;
}

sub deasm ($self, $pc = undef, $n_instructions = undef) {
   my $current_pc = $self->pc;
   $pc //= $self->deasm_pc // $current_pc;
   $n_instructions //= $self->deasm_length;
   my $vm = $self->vm;
   my @rows;
   my $memory = $self->memory;
   my $lf = $self->label_at;
   for (1 .. $n_instructions) {
      push @rows, "$lf->{$pc}[0]:" if exists($lf->{$pc});
      my $opid = $memory->[$pc];
      if ($opid <= OPS->$#*) { # might be a valid op
         my $op = OPS->[$opid];
         if (my $callback = __PACKAGE__->can('deasm_' . $op->{name})) {
            my ($new_pc, @news) = $callback->($vm, $pc);
            $pc = $new_pc;
            push @rows, @news;
         }
         else {
            my $ipc = deasm_ipc($vm, $pc++);
            my @args = map { represent_value($memory->[$pc++]) }
               1 .. $op->{args};
            push @rows, join ' ', $ipc, $op->{name}, @args;
         }
      }
      else {
         my $ipc = deasm_ipc($vm, $pc++);
         push @rows, "$ipc <data>";
         last;
      }
   }
   $self->deasm_pc($pc);
   return @rows;
}

sub deasm_ipc ($vm, $pc) {
   my $is_current = $vm->pc == $pc ? '<' : ' ';
   return sprintf('%6d%s', $pc, $is_current);
}

sub deasm_out ($vm, $pc) {
   my $memory = $vm->memory;
   my $ipc = deasm_ipc($vm, $pc);

   # remove noop along the way
   my $output = '';
   my $has_nops = 0;
   while ('necessary') {
      my $opid = $memory->[$pc++];
      if ($opid == OP_OUT) {
         my $v = represent_value($memory->[$pc++]);
         $output .= $v =~ m{\A\d+\z} ? chr($v) : "<$v>";
      }
      elsif ($opid == OP_NOOP) { $has_nops = 1 } # skip this...
      else { --$pc; last } # backtrack a bit
   }

   # preserve last newline
   my @rows = split m{\n}mxs, $output;
   push @rows, '' if length($output) && substr($output, -1, 1) eq "\n";
   @rows = map { '        | ' . $_ } @rows;
   my $title = $has_nops ? 'out (with no-ops)' : 'out';
   unshift @rows, "$ipc $title:";

   return ($pc, @rows);
}

sub get_args ($vm, $pc, $n) {
   my $memory = $vm->memory;
   my @args = map { represent_value($memory->[$pc++]) } 1 .. $n;
   return ($pc, @args);
}

sub output ($vm, $pcin, $pcout, $msg) {
   return ($pcout, sprintf('%s %s', deasm_ipc($vm, $pcin), $msg));
}

sub deasm_set ($vm, $pcin) {
   my ($pcout, $to, $from) = get_args($vm, $pcin + 1, 2);
   return output($vm, $pcin, $pcout, "$to = $from");
}

# push: ok the default
# pop:  ok the default

sub deasm_eq ($vm, $pcin) {
   my ($pcout, $to, $op1, $op2) = get_args($vm, $pcin + 1, 3);
   return output($vm, $pcin, $pcout, "$to = $op1 == $op2");
}

sub deasm_gt ($vm, $pcin) {
   my ($pcout, $to, $op1, $op2) = get_args($vm, $pcin + 1, 3);
   return output($vm, $pcin, $pcout, "$to = $op1 > $op2");
}

sub deasm_jmp ($vm, $pcin) {
   my ($pcout, $to) = get_args($vm, $pcin + 1, 1);
   return output($vm, $pcin, $pcout, "goto $to");
}

sub deasm_jt ($vm, $pcin) {
   my ($pcout, $cond, $to) = get_args($vm, $pcin + 1, 2);
   return output($vm, $pcin, $pcout, "goto $to if $cond");
}

sub deasm_jf ($vm, $pcin) {
   my ($pcout, $cond, $to) = get_args($vm, $pcin + 1, 2);
   return output($vm, $pcin, $pcout, "goto $to unless $cond");
}

sub deasm_add ($vm, $pcin) {
   my ($pcout, $to, $op1, $op2) = get_args($vm, $pcin + 1, 3);
   return output($vm, $pcin, $pcout, "$to = $op1 + $op2");
}

sub deasm_mult ($vm, $pcin) {
   my ($pcout, $to, $op1, $op2) = get_args($vm, $pcin + 1, 3);
   return output($vm, $pcin, $pcout, "$to = $op1 * $op2");
}

sub deasm_mod ($vm, $pcin) {
   my ($pcout, $to, $op1, $op2) = get_args($vm, $pcin + 1, 3);
   return output($vm, $pcin, $pcout, "$to = $op1 % $op2");
}

sub deasm_and ($vm, $pcin) {
   my ($pcout, $to, $op1, $op2) = get_args($vm, $pcin + 1, 3);
   return output($vm, $pcin, $pcout, "$to = $op1 & $op2");
}

sub deasm_or ($vm, $pcin) {
   my ($pcout, $to, $op1, $op2) = get_args($vm, $pcin + 1, 3);
   return output($vm, $pcin, $pcout, "$to = $op1 | $op2");
}

sub deasm_not ($vm, $pcin) {
   my ($pcout, $to, $op) = get_args($vm, $pcin + 1, 2);
   return output($vm, $pcin, $pcout, "$to = ~ $op");
}

sub deasm_wmem ($vm, $pcin) {
   my ($pcout, $addr, $val) = get_args($vm, $pcin + 1, 2);
   return output($vm, $pcin, $pcout, "mem[$addr] = $val");
}

sub deasm_rmem ($vm, $pcin) {
   my ($pcout, $to, $addr) = get_args($vm, $pcin + 1, 2);
   return output($vm, $pcin, $pcout, "$to = mem[$addr]");
}

sub deasm_in ($vm, $pcin) {
   my ($pcout, $to) = get_args($vm, $pcin + 1, 1);
   return output($vm, $pcin, $pcout, "$to = <in>");
}

1;
