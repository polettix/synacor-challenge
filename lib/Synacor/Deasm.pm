package Synacor::Deasm;
use v5.24;
use Carp;
use English qw< -no_match_vars >;
use experimental qw< signatures >;

use Synacor::Architecture qw< :constants >;

use Exporter qw< import >;
our @EXPORT_OK = qw< deasm >;

sub represent_value ($x) {
   return $x if $x < 0x8000;
   return 'r' . ($x - 0x8000) if $x < 0x8008;
   return '**invalid**';
}

sub deasm ($vm, $pc, $n_instructions = 10) {
   my @rows;
   my $memory = $vm->memory;
   for (1 .. $n_instructions) {
      my $opid = $memory->[$pc];
      if ($opid <= $memory->$#*) { # might be a valid op
         my $op = OPS->[$opid];
         if (my $callback = __PACKAGE__->can('deasm_' . $op->{name})) {
            my ($new_pc, @news) = $callback->($vm, $pc);
            $pc = $new_pc;
            push @rows, @news;
         }
         else {
            my $ipc = sprintf('%6d', $pc++);
            my @args = map { represent_value($memory->[$pc++]) }
               1 .. $op->{args};
            push @rows, join ' ', $ipc, $op->{name}, @args;
         }
      }

   }
   return ($pc, @rows);
}

sub deasm_out ($vm, $pc) {
   my $memory = $vm->memory;
   my $ipc = sprintf('%6d', $pc);

   # remove noop along the way
   my $output = '';
   my $has_nops = 0;
   while ('necessary') {
      my $opid = $memory->[$pc++];
      if ($opid == OP_OUT) {
         $output .= chr(represent_value($memory->[$pc++]));
      }
      elsif ($opid == OP_NOOP) { $has_nops = 1 } # skip this...
      else { --$pc; last } # backtrack a bit
   }

   my @rows = map { '  > ' . $_ } split m{\n}mxs, $output;
   my $title = $has_nops ? 'out (with no-ops)' : 'out';
   unshift @rows, "$ipc $title:";

   return ($pc, @rows);
}

sub get_args ($vm, $pc, $n) {
   my $memory = $vm->memory;
   my @args = map { represent_value($memory->[$pc++]) } 1 .. $n;
   return ($pc, @args);
}

sub output ($pcin, $pcout, $msg) {
   my $ipc = sprintf('%6d', $pcin);
   return ($pcout, sprintf('%6d %s', $pcin, $msg));
}

sub deasm_set ($vm, $pcin) {
   my ($pcout, $to, $from) = get_args($vm, $pcin + 1, 2);
   return output($pcin, $pcout, "$to = $from");
}

sub deasm_add ($vm, $pcin) {
   my ($pcout, $to, $op1, $op2) = get_args($vm, $pcin + 1, 3);
   return output($pcin, $pcout, "$to = $op1 + $op2");
}

sub deasm_mult ($vm, $pcin) {
   my ($pcout, $to, $op1, $op2) = get_args($vm, $pcin + 1, 3);
   return output($pcin, $pcout, "$to = $op1 * $op2");
}

sub deasm_gt ($vm, $pcin) {
   my ($pcout, $to, $op1, $op2) = get_args($vm, $pcin + 1, 3);
   return output($pcin, $pcout, "$to = $op1 > $op2");
}

sub deasm_eq ($vm, $pcin) {
   my ($pcout, $to, $op1, $op2) = get_args($vm, $pcin + 1, 3);
   return output($pcin, $pcout, "$to = $op1 == $op2");
}

sub deasm_jt ($vm, $pcin) {
   my ($pcout, $cond, $to) = get_args($vm, $pcin + 1, 2);
   return output($pcin, $pcout, "goto $to if $cond");
}

sub deasm_jf ($vm, $pcin) {
   my ($pcout, $cond, $to) = get_args($vm, $pcin + 1, 2);
   return output($pcin, $pcout, "goto $to unless $cond");
}

sub deasm_jmp ($vm, $pcin) {
   my ($pcout, $to) = get_args($vm, $pcin + 1, 1);
   return output($pcin, $pcout, "goto $to");
}

sub deasm_wmem ($vm, $pcin) {
   my ($pcout, $addr, $val) = get_args($vm, $pcin + 1, 2);
   return output($pcin, $pcout, "mem[$addr] = $val");
}

sub deasm_rmem ($vm, $pcin) {
   my ($pcout, $to, $addr) = get_args($vm, $pcin + 1, 2);
   return output($pcin, $pcout, "$to = mem[$addr]");
}

sub deasm_in ($vm, $pcin) {
   my ($pcout, $to) = get_args($vm, $pcin + 1, 1);
   return output($pcin, $pcout, "$to = <in>");
}


1;
