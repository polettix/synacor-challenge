package Synacor::VM;
use v5.24;
use Moo;
use English;
use Scalar::Util qw< blessed >;
use experimental qw< signatures >;
use Fcntl;
use Data::Dumper;
use Path::Tiny;
use Cpanel::JSON::XS qw< encode_json decode_json >;

use Synacor::Deasm qw< deasm >;
use Synacor::Architecture qw< :constants >;

# some stuff that might come handy
#use Ouch qw< :trytiny_var >;
#use Try::Catch;
#use Log::Log4perl::Tiny qw< :easy :dead_if_first >;
use namespace::clean;

use constant BUFSIZE => 4096;

has memory     => (is => 'rw', default => sub { [] });
has pc         => (is => 'rw', default => 0);
has registers  => (is => 'rw', default => sub { [ (0) x 8 ] });
has stack      => (is => 'rw', default => sub { [] });
has is_running => (is => 'rw', default => 0);
has full_out   => (is => 'rw', default => 1);
has input      => (is => 'rw', default => '');
has last_out   => (is => 'rw', default => "\n");
has call_stack => (is => 'rw', default => sub { [] });
has auto_dump  => (is => 'rw', default => 1);
has sequences  => (is => 'lazy');
has last_deasm => (is => 'rw', default => undef);

sub _build_sequences ($self) {
   return {
      init => [
         'take tablet',
         'use tablet',
         qw< doorway north north bridge continue down >,
      ],
      ladder => [
         qw< west passage ladder >
      ],
   };
}

sub load ($self, $filename) {
   warn "\n\ndeprecated sub load in $self\n\n";
   return $self->load_binary($filename);
}

sub load_binary ($self, $filename) {
   open my $fh, '<:raw:bytes', $filename
      or die "open('$filename'): $OS_ERROR";
   my @memory;
   while ('necessary') {
      my $n_read = read($fh, my $buffer, BUFSIZE)
         // die "sysread(): $OS_ERROR";
      last unless $n_read; # eof
      push(@memory, unpack('v*', $buffer));
   }
   close $fh;
   push(@memory, (0) x (0x7FFF - @memory));

   $self = $self->new unless blessed($self);
   $self->memory(\@memory);
   return $self;
}

sub load_savefile ($self, $filename) {
   my $restore = decode_json(path($filename)->slurp_raw);
   $self->pc($restore->{pc});
   $self->stack($restore->{stack});
   $self->memory($restore->{memory});
   $self->registers($restore->{registers});
   return $self;
}

sub save_savefile ($self, $filename) {
   my $saver = {
      pc        => $self->pc,
      stack     => $self->stack,
      memory    => $self->memory,
      registers => $self->registers,
   };
   path($filename)->spew_raw(encode_json($saver));
   return $self;
}

sub get_value ($self, $x) {
   return $x if $x < 0x8000;
   return $self->registers->[$self->register($x)];
}

sub set_value ($self, $x, $value) {
   if ($x < 0x8000) { # memory address
      $self->memory->[$x] = $value;
   }
   else { # hopefully a register
      $self->registers->[$self->register($x)] = $value;
   }
   return $self;
}

sub set_register ($self, $x, $value) {
   die "invalid register <$x>" unless 0x8000 <= $x && $x < 0x8008;
   $self->registers->[$x & 0x07] = $value;
}

sub get_register ($self, $x) {
   die "invalid register <$x>" unless 0x8000 <= $x && $x < 0x8008;
   return $self->registers->[$x & 0x07];
}

sub register ($self, $x) {
   die "invalid register <$x>" unless 0x8000 <= $x && $x < 0x8008;
   return $x & 0x07;
}

sub start ($self) {
   $self->pc(0);
   $self->is_running(1);
   $self->stack([]);
   $self->registers([ (0) x 8 ]);
   return $self;
}

sub current_op ($self) { return OPS->[$self->memory->[$self->pc]] }

sub step ($self) {
   return unless $self->is_running;

   my $pc  = $self->pc;
   my $mem = $self->memory;
   my $op  = OPS->[$mem->[$pc++]];
   my @args = map { $mem->[$pc++] } 1 .. $op->{args} // 0;
   $self->pc($pc);

   return $self->can('op_' . $op->{name})->($self, @args) // $self;
}

sub op_halt ($self) {
   $self->is_running(0);
   return $self;
}

sub op_set ($self, $a1, $a2) {
   $self->registers->[$self->register($a1)] = $self->get_value($a2);
   return $self;
}

sub op_push ($self, $a1) {
   push($self->stack->@*, $self->get_value($a1));
   return $self;
}

sub op_pop ($self, $a1) {
   my $stack = $self->stack;
   die "pop: cannot pop from empty stack\n" unless $stack->@*;
   $self->set_register($a1, pop($stack->@*));
   return $self;
}

sub op_eq ($self, $a1, $a2, $a3) {
   my $cmp = $self->get_value($a2) == $self->get_value($a3) ? 1 : 0;
   $self->set_register($a1, $cmp);
   return $self;
}

sub op_gt ($self, $a1, $a2, $a3) {
   my $cmp = $self->get_value($a2) > $self->get_value($a3) ? 1 : 0;
   $self->set_register($a1, $cmp);
   return $self;
}

sub op_jmp ($self, $a1) {
   $self->pc($self->get_value($a1));
   return $self;
}

sub op_jt ($self, $a1, $a2) {
   $self->pc($self->get_value($a2)) if $self->get_value($a1);
   return $self;
}

sub op_jf ($self, $a1, $a2) {
   $self->pc($self->get_value($a2)) unless $self->get_value($a1);
   return $self;
}

sub op_add ($self, $a1, $a2, $a3) {
   my $val = $self->get_value($a2) + $self->get_value($a3);
   $self->set_register($a1, $val % 0x8000);
   return $self;
}

sub op_mult ($self, $a1, $a2, $a3) {
   my $val = $self->get_value($a2) * $self->get_value($a3);
   $self->set_register($a1, $val % 0x8000);
   return $self;
}

sub op_mod ($self, $a1, $a2, $a3) {
   my $val = $self->get_value($a2) % $self->get_value($a3);
   $self->set_register($a1, $val);
   return $self;
}

sub op_and ($self, $a1, $a2, $a3) {
   my $val = $self->get_value($a2) & $self->get_value($a3);
   $self->set_register($a1, $val);
   return $self;
}

sub op_or ($self, $a1, $a2, $a3) {
   my $val = $self->get_value($a2) | $self->get_value($a3);
   $self->set_register($a1, $val);
   return $self;
}

sub op_not ($self, $a1, $a2) {
   my $val = $self->get_value($a2);
   $self->set_register($a1, (~ $val) & 0x7FFF);
   return $self;
}

sub op_rmem ($self, $a1, $a2) {
   #warn "op_rmem pc=", $self->pc - 3;
   my $address = $self->get_value($a2);
   $self->set_register($a1, $self->memory->[$address]);
   return $self;
}

sub op_wmem ($self, $a1, $a2) {
   my $address = $self->get_value($a1);
   $self->memory->[$address] = $self->get_value($a2);
   return $self;
}

sub op_call ($self, $a1) {
   my $target = $self->get_value($a1);
   push($self->call_stack->@*, [$self->pc - 2, $target]);
   push($self->stack->@*, $self->pc);
   $self->pc($target);
   return $self;
}

sub op_ret ($self) {
   pop($self->call_stack->@*);
   my $stack = $self->stack;
   if ($stack->@* == 0) {
      $self->is_running(0);
   }
   else {
      $self->pc(pop($stack->@*));
   }
   return $self;
}

sub op_out ($self, $c) {
   my $full_out = $self->full_out;

   if ($full_out && $self->last_out eq "\n") {
      #say {*STDERR} '-- pc=', $self->pc;
      print {*STDOUT} '>> ';
   }
   
   my $chr = chr($self->get_value($c));
   print {*STDOUT} $chr;

   if ($full_out) {
      my $pc  = $self->pc;
      my $mem = $self->memory;
      while ($mem->[$pc] == OP_OUT) {
         print {*STDOUT} '>> ' if $chr eq "\n";
         $pc++;
         $chr = chr($self->get_value($mem->[$pc++]));
         print {*STDOUT} $chr;
      }
      $self->pc($pc);
   }

   $self->last_out($chr);
   return $self;
}

sub op_in ($self, $a1) {
   my $input = $self->input;

   if (length($input) == 0) {
      $self->cmd_dump if $self->auto_dump;

      while (length($input) == 0) {
         $input = <STDIN>;
         if (! defined($input)) {
            $self->is_running(0);
            return $self;
         }
         last unless substr($input, 0, 1) eq '!';
         substr($input, 0, 1, '');
         last if substr($input, 0, 1) eq '!'; # want to say a literal "!"
         $input =~ s{\A\s+|\s+\z}{}gmxs;
         my ($cmd_name, @args) = split m{\s+}mxs, $input;
         if (my $cmd = $self->can('cmd_' . $cmd_name)) {
            $self->$cmd(@args);
         }
         else {
            say {*STDERR} "-- unsupported command '$cmd_name'";
         }
         $input = $self->input; # might have been reset...
      }
   }

   my $first = substr($input, 0, 1, '');
   $self->input($input);
   $self->set_register($a1, ord($first));
   $self->last_out("\n");
   return $self;
}

sub op_noop ($self) {
   return $self;
}

sub cmd_dump ($self, @args) {
   say {*STDERR} '-- pc<', $self->pc, '>';
   my $registers = $self->registers;
   say {*STDERR} join ' ', map {
      'r' . $_ . '<' . $registers->[$_] . '>'
   } 0 .. 7;
   my @stack = $self->stack->@*;
   splice(@stack, 0, @stack - 4, '...') if @stack > 5;
   for my $frame (reverse($self->call_stack->@*)) {
      my ($from, $to) = $frame->@*;
      say {*STDERR} "-- from $from call to $to";
   }
   say {*STDERR} "-- stk $_" for reverse(@stack);
   return $self;
}

sub cmd_save ($self, @args) {
   my $filename = @args ? $args[0] : 'savefile.json';
   my $saver = {
      pc => $self->pc,
      stack => $self->stack,
      memory => $self->memory,
      registers => $self->registers,
   };
   path($filename)->spew_raw(encode_json($saver));
   say {*STDERR} "-- saved $filename";
   return $self;
}

sub cmd_load ($self, @args) {
   my $filename = @args ? $args[0] : 'savefile.json';
   my $restore = decode_json(path($filename)->slurp_raw);
   $self->pc($restore->{pc});
   $self->stack($restore->{stack});
   $self->memory($restore->{memory});
   $self->registers($restore->{registers});
   say {*STDERR} "-- loaded $filename";
   return $self->cmd_dump;
}

sub cmd_ls ($self, @args) {
   my $where = path(@args ? $args[0] : '.');
   say {*STDERR} "-- $_" for $where->children;
   return $self;
}

sub cmd_auto_dump ($self, @args) {
   my $what = @args ? $args[0] : 1;
   $what = 0 if $what =~ m{\A(?: off | no )\z}imxs;
   $self->auto_dump($what);
   return $self;
}

sub cmd_seq ($self, @args) {
   my $sequences = $self->sequences;
   if (defined(my $name = shift(@args))) {
      if (defined(my $sequence = $sequences->{$name})) {
         my $input = join "\n", $sequence->@*;
         $self->input($input . "\n");
      }
      else {
         say {*STDERR} "-- no such sequence: <$name>";
      }
   }
   else {
      say {*STDERR} "-- $_" for sort keys($sequences->%*);
   }
   return $self;
}

*{cmd_autodump} = \&cmd_auto_dump;

sub cmd_deasm ($self, @args) {
   my $pc = shift(@args) // $self->last_deasm // 'pc';
   $pc = $self->pc - 2 if $pc eq 'pc';
   ($pc, my @rows) = deasm($self, $pc);
   say "-- $_" for @rows;
   $self->last_deasm($pc);
}

*{cmd_p} = \&cmd_deasm;

1;
