package Synacor::Challenge8;
use v5.24;
use Carp;
use English qw< -no_match_vars >;
use experimental qw< signatures >;

# The graph is represented as a matrix, i.e. an array of rows. Rows are
# arranged south to north, columns are arranged west to east. Adapt the
# following values to the specific puzzle inputs.
use constant GRAPH => [
   [ 22, '-', 9, '*' ],
   [ '+', 4, '-', 18 ],
   [ 4, '*', 11, '*' ],
   [ '*', 8, '-', 1  ],
];
use constant TARGET => 30;

# You should not need to go beyond this point.
if (! caller()) {
   if (@ARGV) {
      say reverse($ARGV[0]) =~ tr/pqdb/qpbd/r;
      exit 0;
   }

   my %delta_for = (
      N => [  0,  1 ],
      S => [  0, -1 ],
      E => [  1,  0 ],
      W => [ -1,  0 ],
   );
   my $x_max = GRAPH->[0]->$#*;
   my $y_max = GRAPH->$#*;

   my $best = visit(
      start => {
         path => '',
         value => GRAPH->[0][0],
         x => 0,
         y => 0,
      },
      target_value => TARGET,
      before => sub ($r, $l) {
         my $cmp = length($r->{path}) <=> length($l->{path});
         return $cmp < 0 if $cmp != 0;
         my $vcmp = $r->{value} <=> $l->{value};
         return $vcmp < 0 if $vcmp != 0;
         return $r->{path} lt $l->{path};
      },
      successors => sub ($node) {
         my ($x, $y) = $node->@{qw< x y >};

         # if we're on the target square we don't move further, so there is
         # no successor
         return if $x == $x_max && $y == $y_max;

         my @neighbors;
         for my $dir (qw< N E S W >) {
            my $d = $delta_for{$dir};
            my ($X, $Y) = ($x + $d->[0], $y + $d->[1]);

            next if $X == 0 && $Y == 0; # start square is forbidden

            # ensure we are within allowed bounds
            next unless 0 <= $X && $X <= $x_max && 0 <= $Y && $Y <= $y_max;

            my $item = GRAPH->[$Y][$X];
            my %neighbor = ( # "candidate" neighbor, actually
               path    => ($node->{path} . $dir),
               x       => $X,
               y       => $Y,
               value   => $node->{value},
               item    => $item,
               is_goal => ($X == $x_max && $Y == $y_max),
               min_delta => (($x_max - $X) + ($y_max - $Y)),
            );
            if ($item =~ m{\d}mxs) { # compute new value
               my ($op, $prec) = $node->@{qw< item value >};
               $neighbor{value} =
                  $op eq '-' ? $prec - $item
                  : $op eq '+' ? $prec + $item
                  : $op eq '*' ? $prec * $item
                  :              die "unknown op<$op>";
            }
            # else "op" node, will have to wait for the right-side operand

            # the orb disappears if its weight hits 0 or less
            push @neighbors, \%neighbor if $neighbor{value} > 0;
         }
         return @neighbors;
      },
   );
   say 'best: ', expand_path($best);
   exit 0;
}

sub visit (%args) {
   my $start = $args{start};
   my $target = $args{target_value};
   my $succs = $args{successors};

   # will expand candidate positions based on a priority queue, driven by
   # a heuristic.
   my $pq = __PACKAGE__->new(
      before => $args{before},
      id_of  => sub ($x) { return $x->{path} },
   );
   $pq->enqueue($start);

   my $best = undef;
   while (! $pq->is_empty) {
      my $candidate = $pq->dequeue;
      if (defined($best)) {
         my $min_len = length($candidate->{path}) + $candidate->{min_delta};
         next if $min_len >= $best;
      }
      if ($candidate->{is_goal}) {
         next unless $candidate->{value} == $target;
         $best = $candidate->{path};
         say 'intermediate: ', expand_path($best);
      }
      else { # expand more
         $pq->enqueue($_) for $succs->($candidate);
      }
   }

   return $best;
}

sub expand_path ($path) {
   state $name_for = {
      N => 'north',
      S => 'south',
      E => 'east',
      W => 'west',
   };
   return join ', ', map { $name_for->{$_} } split m{}mxs, $path;
}

sub contains    { return $_[0]->contains_id($_[0]{id_of}->($_[1])) }
sub contains_id { return exists $_[0]{item_of}{$_[1]} }
sub is_empty    { return !$#{$_[0]{items}} }
sub item_of { exists($_[0]{item_of}{$_[1]}) ? $_[0]{item_of}{$_[1]} : () }
sub new;                # see below
sub dequeue { return $_[0]->_remove_kth(1) }
sub enqueue;                # see below
sub remove    { return $_[0]->remove_id($_[0]{id_of}->($_[1])) }
sub remove_id { return $_[0]->_remove_kth($_[0]{pos_of}{$_[1]}) }
sub size      { return $#{$_[0]{items}} }
sub top       { return $_[0]->size ? $_[0]{items}[1] : () }
sub top_id    { return $_[0]->size ? $_[0]{id_of}->($_[0]{items}[1]) : () }

sub new {
   my $package = shift;
   my $self = bless {((@_ && ref($_[0])) ? %{$_[0]} : @_)}, $package;
   $self->{before} ||= sub { return $_[0] < $_[1] };
   $self->{id_of} ||= sub { return ref($_[0]) ? "$_[0]" : $_[0] };
   my $items = $self->{items} || [];
   @{$self}{qw< items pos_of item_of >} = (['-'], {}, {});
   $self->enqueue($_) for @$items;
   return $self;
} ## end sub new

sub enqueue {    # insert + update in one... DWIM
   my ($is, $id) = ($_[0]{items}, $_[0]{id_of}->($_[1]));
   $_[0]{item_of}{$id} = $_[1];    # keep track of this item
   $is->[my $k = $_[0]{pos_of}{$id} ||= $#$is + 1] = $_[1];
   $_[0]->_adjust($k);
   return $id;
} ## end sub enqueue

sub _adjust {                      # assumption: $k <= $#$is
   my ($is, $before, $self, $k) = (@{$_[0]}{qw< items before >}, @_);
   $k = $self->_swap(int($k / 2), $k)
     while ($k > 1) && $before->($is->[$k], $is->[$k / 2]);
   while ((my $j = $k * 2) <= $#$is) {
      ++$j if ($j < $#$is) && $before->($is->[$j + 1], $is->[$j]);
      last if $before->($is->[$k], $is->[$j]);    # parent is OK
      $k = $self->_swap($j, $k);
   }
   return $self;
} ## end sub _adjust

sub _remove_kth {
   my ($is, $self, $k) = ($_[0]{items}, @_);
   die 'no such item' if (!defined $k) || ($k <= 0) || ($k > $#$is);
   $self->_swap($k, $#$is);
   my $r = CORE::pop @$is;
   $self->_adjust($k) if $k <= $#$is;    # no adjust for last element
   my $id = $self->{id_of}->($r);
   delete $self->{$_}{$id} for qw< item_of pos_of >;
   return $r;
} ## end sub _remove_kth

sub _swap {
   my ($self,  $i,      $j)     = @_;
   my ($items, $pos_of, $id_of) = @{$self}{qw< items pos_of id_of >};
   my ($I, $J) = @{$items}[$i, $j] = @{$items}[$j, $i];
   @{$pos_of}{($id_of->($I), $id_of->($J))} = ($i, $j);
   return $i;
} ## end sub _swap

1;
