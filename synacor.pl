#!/usr/bin/env perl
use v5.24;
use warnings;
use English qw< -no_match_vars >;
use experimental qw< signatures >;

use FindBin '$RealBin';
use lib "$RealBin/local/lib/perl5", "$RealBin/lib";

use Synacor::VM;
use Synacor::Dbg;
use Time::HiRes qw< sleep time >;

my ($program) = @ARGV;
$program //= "$RealBin/challenge/challenge.bin";
my $type = $program =~ m{\.json}imxs ? 'debug_file' : 'program_file';
my $dbg = Synacor::Dbg->new($type => $program);
$dbg->start;

__END__


my $start = time();
my $vm = Synacor::VM->load($program);
my $pick  = time();
say sprintf '%.6f', $pick - $start;

$vm->start;
$vm->step while $vm->is_running;
